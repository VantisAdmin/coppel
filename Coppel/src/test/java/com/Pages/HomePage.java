package com.Pages;

import com.Utils.Utils;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.core.pages.PageObject;

public class HomePage extends PageObject  {
	Utils _Util;

	/// WebElements
	@FindBy(xpath="//*[@id=\"header\"]/nav/div/div[1]/div/div/div[2]/div[3]/div[1]/div/span[2]")
	public WebElementFacade cbmIniciarSesion;//Boton iniciar sesion 

	@FindBy(id = "signInQuickLink")
	public WebElementFacade cmbElementoIniciarSesion;//Boton de la lista
	
	// Metodos
	@Step
	public void Ir_login() {
		try {
			cbmIniciarSesion.click();
			_Util.ThreadSleep(2);
			cmbElementoIniciarSesion.click();
			_Util.ThreadSleep(2);
			System.out.print("KUROASHI");
		
		} catch (Exception ex) {
			System.out.print(ex.toString());
		}
	}


}
