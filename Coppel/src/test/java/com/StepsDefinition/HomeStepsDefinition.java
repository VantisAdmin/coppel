package com.StepsDefinition;

import com.Pages.HomePage;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class HomeStepsDefinition {
	@Steps
	HomePage _HomePage;

	@Given("^Ir login$")
	public void Ir_login() throws Exception {
		_HomePage.Ir_login();
	}
}
